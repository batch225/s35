// ! Activity Codes:

// Loads the Express library, which is a web application framework for Node.js.
const express = require("express");

// loads the Mongoose library, which is a MongoDB object modeling tool for Node.js.
const mongoose = require("mongoose");

// Creates an Express application
const app = express();

// Sets the server to listen on port 3000
const port = 3000;

// Uses express.json() middleware to parse incoming request bodies as JSON
app.use(express.json());

// Uses express.urlencoded({extended:true}) middleware to parse incoming request bodies as url-encoded data.
app.use(express.urlencoded({extended:true}));

// Connects to a MongoDB database using the Mongoose library. The connection string is the URL to the MongoDB instance.
mongoose.connect("mongodb+srv://ronb077:6n_CZhCfmq4MnNX@cluster0.umwequr.mongodb.net/s35_Activity?retryWrites=true&w=majority", {	
        useNewUrlParser : true,
        useUnifiedTopology : true
});

// Creates a variable 'db' that is set to the Mongoose connection object.
let db = mongoose.connection;

// Sets an event listener on the 'error' event of the 'db' connection object. If there is an error with the connection, it will be logged to the console.
db.on("error", console.error.bind(console, "connection error"));

// Sets an event listener on the 'open' event of the 'db' connection object. If the connection is successful, a message will be printed to the console.
db.on("open", () => console.log("Connected to MongoDB!"));

// Creates a new Mongoose schema for a 'User' collection, with a 'username' and 'password' fields.
const userSchema = new mongoose.Schema({ 
	username : String,
    password : String,
});

// Creates a Mongoose model for the 'User' schema.
const User = mongoose.model("User", userSchema);

// Creates an endpoint 'signup' on the Express app that listens for a POST request.
app.post("/signup", (req, res) => {

	// Finds a user with the same username in the MongoDB collection.
	User.findOne({username : req.body.username}, (err, result) => {

		// Checks if the result is not null and if the result has the same username as the request body's username. If true, it sends back a message 'Duplicate user found'
		if(result != null && result.username == req.body.username){

			return res.send("Duplicate user found");

		} else {

			// Creates a new user using the request body's username and password.
			let newUser = new User({
				username : req.body.username,
                password : req.body.password
			});

			// Saves the new user to the MongoDB collection.
			newUser.save((saveErr, savedUser) => {

				// Checks if there is an error saving the user and logs the error to the console. If the user is saved successfully, it sends back a message 'New user registered'
				if(saveErr){

					return console.error(saveErr);

				} else {

					return res.status(201).send("New user registered");

				}
			})
		}

	})
});

// Starts the Express server and logs a message to the console when the server is running.
app.listen(port, () => console.log(`Server running at ${port}`));